from typing import List


def convert_fax_account_number_to_string(entry: str):
    if entry.lstrip("\n")[:27] == '                           ':
        return "111111111"
    return "000000000"


class AsciiNumber:
    def __init__(self, ascii_number):
        self.ascii_number = ascii_number


class SplitAscii:
    ascii_number_width = 3

    def __init__(self, ascii_account_number: str):
        self.ascii_account_number = ascii_account_number

    def get_ascii_at_index(self, index: int) -> AsciiNumber:
        ascii_index = index * self.ascii_number_width
        ascii_rows = self._split_ascii_in_rows()
        sliced_rows = [self._get_selected_row_slice(row, ascii_index)
                       for row in ascii_rows]
        ascii_number = "\n".join(sliced_rows)
        return AsciiNumber(ascii_number)

    def _split_ascii_in_rows(self) -> List[str]:
        return self.ascii_account_number.split("\n")

    def _get_selected_row_slice(self, row: str, ascii_index: int) -> str:
        return row[ascii_index:ascii_index + self.ascii_number_width]
