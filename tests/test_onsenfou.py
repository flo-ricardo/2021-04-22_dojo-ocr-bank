import pytest

from onsenfou import __version__
from onsenfou.onsenfou import convert_fax_account_number_to_string, SplitAscii


def test_version():
    assert __version__ == '0.1.0'


def test_onsenfou():
    assert True is True


@pytest.mark.parametrize("entry,expected", [
    ("""
 _  _  _  _  _  _  _  _  _ 
| || || || || || || || || |
|_||_||_||_||_||_||_||_||_|

""", "000000000"),
    ("""
                           
  |  |  |  |  |  |  |  |  |
  |  |  |  |  |  |  |  |  |

""", "111111111")
])
def test_given_9_ascii_numbers_return_9_string_numbers(entry, expected):
    # arrange
    # act
    result = convert_fax_account_number_to_string(entry)
    # assert
    assert result == expected


@pytest.mark.parametrize("index,expected", [
    (0, """
 _ 
| |
|_|

"""),
    (1, """
 _ 
  |
  |

""")

])
def test_given_9_ascii_digit_get_one_specific_digit_separatly(index: int, expected: str):
    # arrange
    input = """
 _  _  _  _  _  _  _  _  _ 
| |  || || || || || || || |
|_|  ||_||_||_||_||_||_||_|

"""
    # act
    result = SplitAscii(input.lstrip("\n")).get_ascii_at_index(index)
    # assert
    assert result.ascii_number == expected.lstrip("\n")


def test_given_one_ascii_number_return_number():
    # arrange
    pass

    # act

    # assert
